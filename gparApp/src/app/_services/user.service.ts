import { Injectable } from '@angular/core';

import { User } from '../_model/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class UserService {

  // loggedUser: Subject<User> = new Subject<User>();

  path = '/public';

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  /**
   * @desc Invoke Rest API for user login
   * @param user
   */
  login(user: User) {

    return this.http.post(environment.apiURL.concat(this.path).concat('/login'), user).pipe(
      map((result: any) => {
        if (result !== null) {
          // set user logged
          // this.loggedUser.next(user);
          localStorage.setItem('access_token', result.accessToken);
          this.auth.setLoggedIn(true);
          localStorage.setItem('current_user', user.restaurantName);
          localStorage.setItem('current_email', result.emailManager);
          return true;
        }

        const error = result.error.message || result.status;
        return throwError(error);
      })
    );
  }

  /**
   * @desc Removes the token from the localstorage
   * @memberof AuthenticationService
   */
  logout() {
    localStorage.removeItem('access_token');
  }

  /**
   * @desc Invoke Rest API to save new account
   * @param user
   */
  saveUser(user: User) {

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post(environment.apiURL.concat(this.path).concat('/accounts'),
      JSON.stringify(user), {headers: headers, responseType: 'text'});
  }

  // private handleError(error: any): Promise<string> {
  //   console.error('An error occurred', error);
  //   return Promise.reject(error.message || error);
  // }
}
