import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserService} from './user.service';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

/**
 * Prevent access to routes if access-token is not present.
 *
 * @export
 * @class AuthGuard
 * @implements {CanActivate}
 */
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private auth: AuthService) { }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
    if (this.auth.isLoggedIn) {
      return true;
    }

    if (localStorage.getItem('access_token')) {
      console.log('get access token')
      return true;
    }
    this.router.navigate(['login']);
    console.log('Access Token required')
    return false;
}

// canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
//
//   if (localStorage.getItem('access_token')) {
//     console.log('get access token')
//     return true;
//   }
//   this.router.navigate(['login']);
//   console.log('Access Token required')
//   return false;
// }


}
