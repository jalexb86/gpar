import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class JwtInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // get jwt token
    const token = localStorage.getItem('access_token');
    console.log('token interceptor: ' + token)

    if (!token) {
      // if there is no token, proceed with normal request
      console.log('Token not enabled')
      return next.handle(req);
    }

    // add authorization header with jwt token if available
    const with_auth_request = req.clone({
      headers: req.headers.set('Authorization', `Bearer ${token}`),
    });

    // if there is a jwt token, include that to the request header and then send it.
    return next.handle(with_auth_request);
  }


}
