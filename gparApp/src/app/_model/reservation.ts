export class Reservation {
  idReservation: number;
  idAccount: number;
  dateTimeC: string;
  nameC: string;
  surnameC: string;
  emailCustomer: string;
  telC: string;
  note: string;
  numberOfPeople: number;
}
