import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { Reservation } from '../../_model/reservation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  reservations: Reservation[];

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.getReservations().subscribe(res => this.reservations = res);
  }

}
