import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MenuComponent} from './menu/menu.component';
import {RouterModule} from '@angular/router';
import {AuthService} from '../_services/auth.service';
import {HttpClientModule} from '@angular/common/http';
import {AuthGuardService} from '../_services/auth-guard.service';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';

@NgModule({
  declarations: [MenuComponent, HomeComponent, AboutComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [
    AuthService,
    // {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true},
    AuthGuardService
  ]
})
export class AuthModule { }
