import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../_services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  user: string;

  constructor(private authService: AuthService, private router: Router) {

  }


  ngOnInit() {

    // get user logged from session
    this.user = sessionStorage.getItem('current_user');
    this.authService.getReservations();
  }

}
