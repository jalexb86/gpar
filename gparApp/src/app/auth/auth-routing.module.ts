import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../_services/auth-guard.service';
import { MenuComponent } from './menu/menu.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {path: 'gpar/auth', component: MenuComponent, canActivate: [AuthGuardService], children: [
      {path: 'home',  component: HomeComponent, canActivate: [AuthGuardService]},
      {path: 'about', component: AboutComponent, canActivate: [AuthGuardService]}
    ]}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class AuthRoutingModule { }
