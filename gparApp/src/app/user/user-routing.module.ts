import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';

const routes: Routes = [
  {path: '', redirectTo: 'gpar/login', pathMatch: 'full'},
  {path: 'gpar/login', component: LoginComponent},
  {path: 'gpar/signup', component: SignupComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class UserRoutingModule { }
