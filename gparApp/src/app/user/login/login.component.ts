import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as $ from 'jquery';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../_services/user.service';
import {User} from '../../_model/user';
import {first} from 'rxjs/operators';
import {AuthService} from '../../_services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userForm: FormGroup;
  showError: boolean;
  user = new User();

  constructor(private userService: UserService, private fb: FormBuilder, private router: Router, private route: ActivatedRoute, private auth: AuthService) {

  }

  ngOnInit() {
    this.showError = false;
    this.userForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.route.params.subscribe(() => {

      $('.col-md-10 .input-effect input').val('');

      $('.input-effect input').focusout(function() {
        if ($(this).val() !== '') {
          $(this).addClass('has-content');
        } else {
          $(this).removeClass('has-content');
        }
      });
    });
  }

  onLogin(form: FormGroup) {

    console.log('Username: ', form.value.username);
    console.log('Password: ', form.value.password);

    this.user.restaurantName = form.value.username;
    this.user.password = form.value.password;

    this.userService.login(this.user).pipe(first()).subscribe(
      loggedIn => {
        if (loggedIn) {
          this.auth.setLoggedIn(true);
          this.router.navigate(['gpar/auth/home']);
        }
      },
      error => {
        console.log(error);
        this.showError = true;
      }
    );
  }
}
