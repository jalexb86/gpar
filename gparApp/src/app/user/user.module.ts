import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../_services/user.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptorService } from '../_services/jwt-interceptor.service';
import { ErrorInterceptorService } from '../_services/error-interceptor.service';
import { AuthGuardService } from '../_services/auth-guard.service';
import { SignupComponent } from './signup/signup.component';
import { FieldErrorComponent } from './field-error/field-error.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UserService,
    // {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptorService, multi: true},
    // {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true},
    AuthGuardService
  ],

  declarations: [ LoginComponent, SignupComponent, FieldErrorComponent]
})
export class UserModule { }
