import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../_services/user.service';
import * as $ from 'jquery';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {PasswordValidator} from '../PasswordValidator';
import {User} from '../../_model/user';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  user = new User();
  showError: boolean;
  showSuccess: boolean;
  message: string;

  constructor(private userService: UserService, private activatedRoute: ActivatedRoute, private formBuilder: FormBuilder) {

  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
        restaurantName: ['', Validators.required],
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.required,
                                                     Validators.minLength(8),
                                                     Validators.maxLength(20)])],
        confirmPassword: ['', Validators.compose([Validators.required,
                                                            Validators.minLength(8),
                                                            Validators.maxLength(20),
          PasswordValidator.MatchPassword])]
      });

    this.activatedRoute.params.subscribe( () => {
      $('.col-md-12 .input-effect input').val('');

      $('.input-effect input').focusout(function () {
        if ($(this).val() !== '') {
          $(this).addClass('has-content');
        } else {
          $(this).removeClass('has-content');
        }
      });
    });
  }

  /**
   * @desc check if form is valid and submit form.
   *       Otherwise show list of errors.
   */
  save() {
    if (this.registerForm.invalid) {
      this.formErrors(this.registerForm);
    } else {
      this.user.restaurantName = this.registerForm.get('restaurantName').value;
      this.user.emailManager = this.registerForm.get('email').value;
      this.user.password = this.registerForm.get('password').value;

      this.userService.saveUser(this.user).subscribe(
        data => {
          const dt = JSON.parse(data);
         if (dt.responseCode === 200) {
           this.showSuccess = true;
           this.showError = false;
         } else {
           this.showSuccess = false;
           this.showError = true;
         }
         this.message = dt.message;
        },
        err => console.log('An error occurred: ' + err));
    }
  }

  /**
   * @desc check form's control fields
   */
  formErrors(form: FormGroup) {

    Object.keys(form.controls).forEach( field => {
      const control = form.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.formErrors(control);
      }
    });
  }

  /**
   * @desc check if field is valid
   * @param field
   */
  isFieldValid(field: string) {
    return !this.registerForm.get(field).valid && this.registerForm.get(field).touched;
  }

  /**
   * @desc set field's border-color
   * @param field
   */
  displayFieldCss(field: string) {
    return {
      'control-field-error': this.isFieldValid(field)
    };
  }
}
