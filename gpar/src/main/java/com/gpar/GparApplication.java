package com.gpar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.gpar.model")
@EnableJpaRepositories("com.gpar.repository")
public class GparApplication {

    public static void main(String[] args) {

        SpringApplication.run(GparApplication.class, args);
    }

}

