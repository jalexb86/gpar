package com.gpar.controller;

import com.gpar.dto.AccountDto;
import com.gpar.dto.LoginDto;
import com.gpar.model.ApiResponse;
import com.gpar.model.JwtAuthenticationResponseDTO;
import com.gpar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/api/v1/gpar/public")
public class AccountController {

    private static final Logger LOGGER = Logger.getLogger(AccountController.class.getName());

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private AccountService accountService;

    @PostMapping(value = "/accounts")
    public ApiResponse saveAccount(@Valid @RequestBody final AccountDto accountDto)  {

        ApiResponse apiResponse = null;

        if(accountService.existByEmailManager(accountDto.getEmailManager())) {
            LOGGER.warning("User " + accountDto.getEmailManager() + " exist in database yet.");
            apiResponse = new ApiResponse(HttpStatus.FOUND.value(), HttpStatus.FOUND,
                    messageSource.getMessage("accounts.found", null, LocaleContextHolder.getLocale()));
        }else {
            try {
                AccountDto account = accountService.saveAccount(accountDto);
                if (account != null) {
                    LOGGER.info("User " + account.getEmailManager() + " inserted successfully.");
                    apiResponse = new ApiResponse(HttpStatus.OK.value(), HttpStatus.OK,
                            messageSource.getMessage("accounts.save", null , LocaleContextHolder.getLocale()));

                }
            } catch (Exception e) {
                LOGGER.severe(" ----- SaveAccount method failed. -----" + e);
                apiResponse = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR,
                        messageSource.getMessage("generic.fail", null, LocaleContextHolder.getLocale()));
            }
        }

        return apiResponse;
    }

    @PostMapping(value = "/login")
    public ResponseEntity<?> login(@Valid @RequestBody final LoginDto loginDto) {

        LOGGER.info(loginDto.getRestaurantName() + " "+ loginDto.getPassword());

        JwtAuthenticationResponseDTO jwt;

        try {
            jwt = accountService.login(loginDto);
        }catch (Exception ex){
            LOGGER.severe("----- Bad Credentials ----- " +ex);
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(ex);
        }

        return ResponseEntity.ok(jwt);
    }
}

