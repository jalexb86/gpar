package com.gpar.controller;

import com.gpar.dto.ReservationDto;
import com.gpar.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/api/v1/gpar/auth/")
public class ReservationController {

    private static final Logger LOGGER = Logger.getLogger(ReservationController.class.getName());

    @Autowired
    private ReservationService reservationService;

    @GetMapping(value = "reservations/{email}")
    public List<ReservationDto> getReservationsByAccountId(@PathVariable String email) {
        LOGGER.info("getReservationsByAccountId method called.");

        return reservationService.findAllReservationByEmailManager(email);
    }
}
