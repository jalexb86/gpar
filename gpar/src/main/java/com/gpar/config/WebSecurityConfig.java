package com.gpar.config;

import com.gpar.security.JwtAuthenticationFilter;
import com.gpar.security.JwtAuthenticationHandlerUnauthorized;
import com.gpar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true,
                            prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccountService accountService;

    @Autowired
    private JwtAuthenticationHandlerUnauthorized unauthorizedHandler;

//    @Autowired
//    private CorsFilter corsFilter;

//    @Bean
//    CorsFilter corsFilter() {
//        return new CorsFilter();
//    }


    @Override
    protected void configure(final HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
               // .addFilterBefore(corsFilter, ChannelProcessingFilter.class)
                .authorizeRequests()
                    .antMatchers("/api/v1/gpar/public/*")
                        .permitAll()
                    .antMatchers("/resources/*")
                        .permitAll()
                    .antMatchers("/api/v1/gpar/auth/*")
                        .authenticated()
                    .and()
                    .   exceptionHandling().authenticationEntryPoint(this.unauthorizedHandler)
                    .and()
                        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);


    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(accountService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter(){
        return new JwtAuthenticationFilter();
    }
}
