package com.gpar.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "RESERVATION", schema = "GPAR")
public class Reservation implements Serializable {

    @Id
    @Column(name = "ID_RESERVATION", nullable = false)
    @GenericGenerator(name = "gpar.gpar.reservation_seq",
                      strategy = "sequence",
                      parameters = {
                           @org.hibernate.annotations.Parameter(name = "sequence_name", value = "reservation_seq")
                        }
    )
    @GeneratedValue(generator = "gpar.gpar.reservation_seq", strategy = GenerationType.SEQUENCE)
    private Long idReservation;

    @ManyToOne
    @JoinColumn(name = "IDACCOUNT")
    private Account account;

    @Basic
    @Column(name = "EMAIL_CUSTOMER", nullable = false, length = 50)
    private String emailCustomer;

    @Basic
    @Column(name = "NAME_C", nullable = false, length = 30)
    private String nameC;

    @Basic
    @Column(name = "SURNAME_C", nullable = false, length = 50)
    private String surnameC;

    @Basic
    @Column(name = "TEL_C", nullable = false, length = 50)
    private String telC;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATETIME_C", nullable = false)
    private Date dateTimeC;

    @Basic
    @Column(name = "NUMBER_OF_PEOPLE", nullable = false, precision = 0)
    private long numberOfPeople;

    @Basic
    @Column(name = "NOTE", length = 500)
    private String note;

    @Basic
    @Column(name = "NEW_C", nullable = false, length = 10)
    private String newC;

}
