package com.gpar.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "ACCOUNT", schema = "GPAR")
public class Account implements Serializable {


    @Id
    @Column(name = "ID_ACCOUNT", nullable = false)
    @GenericGenerator(name = "gpar.gpar.account_seq",
                      strategy = "sequence",
                      parameters = {
                            @org.hibernate.annotations.Parameter(name = "sequence", value = "account_seq")
                        })
    @GeneratedValue(generator = "gpar.gpar.account_seq", strategy = GenerationType.SEQUENCE)
    private Long idAccount;

    @Column(name = "EMAIL_MANAGER", nullable = false, length = 50)
    private String emailManager;

    @Basic
    @Column(name = "NAME_RESTAURANT", nullable = false, length = 50)
    private String restaurantName;

    @Basic
    @Column(name = "PASSWORD", nullable = false, length = 40)
    private String password;

    @OneToMany(mappedBy = "account")
    private List<Reservation> reservations;

}
