package com.gpar.model;

import com.gpar.dto.AccountDto;
import lombok.Data;

@Data
public class JwtAuthenticationResponseDTO extends AccountDto {

    private String accessToken;
    private String token = "Bearer";

    public JwtAuthenticationResponseDTO(String accessToken, String email, String username, String password) {
        super(email, username, password);
        this.accessToken = accessToken;
    }
}
