package com.gpar.repository;

import com.gpar.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByAccount_EmailManager(String emailManager);

}
