package com.gpar.repository;

import com.gpar.model.Account;
import com.gpar.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByRestaurantName(String nameRestaurant);

    boolean existsByEmailManager(String emailManager);

    Optional<Account> findByEmailManager(String emailManager);
}
