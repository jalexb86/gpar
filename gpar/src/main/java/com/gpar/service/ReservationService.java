package com.gpar.service;

import com.gpar.dto.ReservationDto;
import com.gpar.mapper.ReservationMapper;

import java.util.List;

public interface ReservationService {
    List<ReservationDto> findAllReservationByEmailManager(String emailManager);
}
