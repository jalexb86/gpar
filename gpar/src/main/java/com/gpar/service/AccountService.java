package com.gpar.service;

import com.gpar.dto.AccountDto;
import com.gpar.dto.LoginDto;
import com.gpar.model.JwtAuthenticationResponseDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AccountService extends UserDetailsService {

    AccountDto saveAccount(AccountDto accountDto) throws Exception;

    boolean existByEmailManager(String emailManager);

    UserDetails loadUserByEmailManager(String email);

    JwtAuthenticationResponseDTO login(LoginDto loginDto) throws Exception;

}
