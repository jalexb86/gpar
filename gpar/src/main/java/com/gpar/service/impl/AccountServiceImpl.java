package com.gpar.service.impl;

import com.gpar.dto.AccountDto;
import com.gpar.dto.LoginDto;
import com.gpar.mapper.AccountMapper;
import com.gpar.model.Account;
import com.gpar.model.JwtAuthenticationResponseDTO;
import com.gpar.repository.AccountRepository;
import com.gpar.security.JwtTokenProvider;
import com.gpar.security.UserPrincipal;
import com.gpar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;


    @Override
    public AccountDto saveAccount(final AccountDto accountDto) throws Exception {

        accountDto.setPassword(passwordEncoder.encode(accountDto.getPassword()));

        final Account account = AccountMapper.MAPPER.accountDtoToAccount(accountDto);
        final Account accountSaved = accountRepository.save(account);

        return AccountMapper.MAPPER.accountToAccountDto(accountSaved);
    }

    @Override
    public boolean existByEmailManager(final String emailManager) {
        return accountRepository.existsByEmailManager(emailManager);
    }

    @Override
    public JwtAuthenticationResponseDTO login(LoginDto loginDto){

        //passing to UsernamePasswordAuthentication Filter
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getRestaurantName(), loginDto.getPassword()));

        //sets authorized user context.
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // get user logged's data
        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        //creates jwt token
        String token = tokenProvider.generateToken(authentication);

        return new JwtAuthenticationResponseDTO(token, userPrincipal.getEmail(), userPrincipal.getUsername(), userPrincipal.getPassword());
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Account account = accountRepository.findByRestaurantName(username);
        if(account == null)
            throw new UsernameNotFoundException("Username non trovato.");

        return UserPrincipal.create(account);
    }

    // This method is used by JWTAuthenticationFilter
    @Transactional
    public UserDetails loadUserByEmailManager(String email) {
        Account user = accountRepository.findByEmailManager(email).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + email)
        );

        return UserPrincipal.create(user);
    }

}
