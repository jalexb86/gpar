package com.gpar.service.impl;

import com.gpar.dto.ReservationDto;
import com.gpar.mapper.ReservationMapper;
import com.gpar.model.Reservation;
import com.gpar.repository.ReservationRepository;
import com.gpar.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Override
    public List<ReservationDto> findAllReservationByEmailManager(String email) {
        List<ReservationDto> list = new ArrayList<>();
        List<Reservation> reservation = reservationRepository.findByAccount_EmailManager(email);
        reservation.forEach(l -> list.add(ReservationMapper.RESERVATION_MAPPER.reservationToReservationDto(l)));

        return list;
    }

}
