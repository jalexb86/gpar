package com.gpar.mapper;

import com.gpar.dto.ReservationDto;
import com.gpar.model.Reservation;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ReservationMapper {

    ReservationMapper RESERVATION_MAPPER = Mappers.getMapper(ReservationMapper.class);

    @Mappings({
            @Mapping(target = "idReservation", source = "idReservation"),
            @Mapping(target = "idAccount", source = "account.idAccount"),
            @Mapping(target = "emailManager", source = "account.emailManager"),
            @Mapping(target = "emailCustomer", source = "emailCustomer"),
            @Mapping(target = "nameC", source = "nameC"),
            @Mapping(target = "surnameC", source = "surnameC"),
            @Mapping(target = "telC", source = "telC"),
            @Mapping(target = "dateTimeC", source = "dateTimeC"),
            @Mapping(target = "numberOfPeople", source = "numberOfPeople"),
            @Mapping(target = "note", source = "note"),
            @Mapping(target = "newC", source = "newC")
    })
    ReservationDto reservationToReservationDto(Reservation entity);

    @InheritInverseConfiguration(name = "reservationToReservationDto")
    Reservation reservationDtoToReservation(ReservationDto reservationDto);

}
