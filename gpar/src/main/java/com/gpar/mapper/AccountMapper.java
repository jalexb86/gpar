package com.gpar.mapper;

import com.gpar.dto.AccountDto;
import com.gpar.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AccountMapper {

    AccountMapper MAPPER = Mappers.getMapper(AccountMapper.class);

    @Mappings({
            @Mapping(target = "emailManager", source = "entity.emailManager"),
            @Mapping(target = "restaurantName", source = "entity.restaurantName"),
            @Mapping(target = "password", source = "entity.password")
    })
    AccountDto accountToAccountDto(Account entity);

    @Mappings({
            @Mapping(target = "emailManager", source = "dto.emailManager"),
            @Mapping(target = "restaurantName", source = "dto.restaurantName"),
            @Mapping(target = "password", source = "dto.password")
    })
    Account accountDtoToAccount(AccountDto dto);

}
