package com.gpar.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AccountDto {

    private String emailManager;
    private String restaurantName;
    private String password;
}
