package com.gpar.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class ReservationDto {

    private Long idReservation;
    private Long idAccount;
    private String emailManager;
    private String emailCustomer;
    private String nameC;
    private String surnameC;
    private String telC;
    private Date dateTimeC;
    private Integer numberOfPeople;
    private String note;
    private String newC;
}
