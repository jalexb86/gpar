package com.gpar.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gpar.model.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserPrincipal implements UserDetails {

    // for serializing & deserializing the object
    private static final long serialVersionUID = -53735551720424755L;

    private String username;

    @JsonIgnore // incase this is send a json response. this field will be ignored.
    private String email;

    @JsonIgnore
    private String password;

    public UserPrincipal(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    //Convert UserEntity to UserPrincipal
    public static UserPrincipal create(Account account) {
        return new UserPrincipal(account.getRestaurantName(), account.getEmailManager(), account.getPassword());
    }


    public String getEmail() {
        return email;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }


    @Override
    public boolean isAccountNonExpired() {
        return true; // account doesn't expire
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; // account won't get locked
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; // account creds won't get expired
    }

    @Override
    public boolean isEnabled() {
        return true; // is activated
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }


}
