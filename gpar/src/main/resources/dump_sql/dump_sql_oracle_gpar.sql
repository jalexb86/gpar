create table GPAR.ACCOUNT
(
  EMAIL_MANAGER   VARCHAR2(50) not null
    constraint ACCOUNT_PK
      primary key,
  NAME_RESTAURANT VARCHAR2(50) not null,
  PASSWORD        VARCHAR2(40) not null
)
/

create table GPAR.RESERVATION
(
  ID_RESERVATION   NUMBER        not null
    constraint RESERVATION_PK
      primary key,
  EMAIL_MANAGER    VARCHAR2(50)  not null,
  EMAIL_CUSTOMER   VARCHAR2(50)  not null,
  NAME_C           VARCHAR2(30)  not null,
  SURNAME_C        VARCHAR2(50)  not null,
  TEL_C            VARCHAR2(50)  not null,
  TIME_BOOKING     NUMBER        not null,
  DATE_BOOKING     VARCHAR2(11)  not null,
  NUMBER_OF_PEOPLE NUMBER        not null,
  NOTE             VARCHAR2(500) not null,
  NEW_C            VARCHAR2(10)  not null
)
/





