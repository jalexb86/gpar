DROP table if exists gpar.ACCOUNT;

create table gpar.ACCOUNT
(
    ID_ACCOUNT integer not null
        constraint account_pk
            primary key,
    EMAIL_MANAGER   VARCHAR(50) not null,
    NAME_RESTAURANT VARCHAR(50) not null,
    PASSWORD        VARCHAR(250) not null
);

drop table if exists gpar.RESERVATION;

create table gpar.RESERVATION
(
    ID_RESERVATION   integer        not null
        constraint RESERVATION_PK
            primary key,
    EMAIL_CUSTOMER    VARCHAR(50)  not null,
    IDACCOUNT		   INTEGER      not null,
    NAME_C           VARCHAR(30)  not null,
    SURNAME_C        VARCHAR(50)  not null,
    TEL_C            VARCHAR(50)  not null,
    DATETIME_C	   timestamp    not null,
    NUMBER_OF_PEOPLE INTEGER      not null,
    NOTE             VARCHAR(500),
    NEW_C            VARCHAR(10)  not null
);
-- DROP SEQUENCE gpar.reservation_seq;

CREATE SEQUENCE gpar.reservation_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1
    NO CYCLE;

-- Permissions

ALTER SEQUENCE gpar.reservation_seq OWNER TO alessandro;
GRANT ALL ON SEQUENCE gpar.reservation_seq TO alessandro;

-- DROP SEQUENCE gpar.account_seq;

CREATE SEQUENCE gpar.account_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1
    NO CYCLE;

-- Permissions

ALTER SEQUENCE gpar.account_seq OWNER TO alessandro;
GRANT ALL ON SEQUENCE gpar.account_seq TO alessandro;

-- CREATE UNIQUE INDEX account_pk ON account USING btree (ID_ACCOUNT);
-- CREATE UNIQUE INDEX reservation_pk ON reservation USING btree (ID_RESERVATION);
